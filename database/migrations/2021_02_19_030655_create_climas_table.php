<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClimasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('climas', function (Blueprint $table) {
            $table->id();
            $table->string('tipo');
            $table->timestamps();
        });

        DB::table('climas')->insert([
           ['tipo' => 'soleado'],
           ['tipo' => 'soleado parcial'],
           ['tipo' => 'nublado'],
           ['tipo' => 'lluvia parcial'],
           ['tipo' => 'lluvioso'],
           ['tipo' => 'tormenta'],
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('climas');
    }
}
