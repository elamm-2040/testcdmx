<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePronosticosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pronosticos', function (Blueprint $table) {
            $table->id();
            $table->foreignId('id_ciudad');
            $table->foreignId('id_climas');
            $table->dateTime('fecha_hora_inicial');
            $table->dateTime('fecha_hora_final');
            $table->integer('porcentaje_lluvia');
            $table->index('id_ciudad');
            $table->index('id_climas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pronosticos');
    }
}
