<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix('ciudad')->group(function(){
    Route::get('{id}', [\App\Http\Controllers\CiudadController::class, 'get']);
    Route::post('', [\App\Http\Controllers\CiudadController::class, 'set']);
    Route::get('', [\App\Http\Controllers\CiudadController::class, 'all']);
    Route::delete('{id}', [\App\Http\Controllers\CiudadController::class, 'delete']);
    Route::post('edit', [\App\Http\Controllers\CiudadController::class, 'edit']);
});

Route::prefix('pronosticos')->group(function(){
    Route::get('{id}', [\App\Http\Controllers\PronosticosController::class, 'get']);
    Route::get('ciudad/{id}', [\App\Http\Controllers\PronosticosController::class, 'ciudad']);
    Route::post('', [\App\Http\Controllers\PronosticosController::class, 'set']);
    Route::get('', [\App\Http\Controllers\PronosticosController::class, 'all']);
    Route::delete('{id}', [\App\Http\Controllers\PronosticosController::class, 'delete']);
    Route::post('edit', [\App\Http\Controllers\PronosticosController::class, 'edit']);
});
