<?php

namespace App\Http\Controllers;

use App\Ciudad;
use Illuminate\Http\Request;

class CiudadController extends Controller
{
    public function get($id)
    {
        $ciudad = Ciudad::findOrFail($id)->with('pronosticos');

        return response()->json($ciudad);
    }

    public function all()
    {
        $ciudades = Ciudad::all();

        return response()->json($ciudades);
    }

    public function set(Request $request)
    {
        $ciudad = new Ciudad;
        $ciudad->nombre = $request->nombre;
        $ciudad->estado = $request->estado;
        $ciudad->pais = $request->pais;
        $ciudad->save();
    }

    public function edit(Request $request)
    {
        $ciudad = Ciudad::findOrFail($request->id);
        $ciudad->nombre = $request->nombre;
        $ciudad->estado = $request->estado;
        $ciudad->pais = $request->pais;
        $ciudad->save();
    }

    public function delete($id)
    {
        $ciudad = Ciudad::findOrFail($id);
        $ciudad->delete();
    }
}
