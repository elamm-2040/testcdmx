<?php

namespace App\Http\Controllers;

use App\Pronosticos;
use Illuminate\Http\Request;

class PronosticosController extends Controller
{
    public function get($id)
    {
        $prons = Pronosticos::findOrFail($id)->with('ciudad', 'climas');

        return response()->json($prons);
    }

    public function all()
    {
        $prons = Pronosticos::all()->with('ciudad', 'climas');

        return response()->json($prons);
    }

    public function set(Request $request)
    {
        $prons = new Pronosticos;
        $prons->id_ciudad = $request->id_ciudad;
        $prons->id_climas = $request->id_climas;
        $prons->fecha_hora_inicial = $request->fecha_hora_inicial;
        $prons->fecha_hora_final = $request->fecha_hora_final;
        $prons->procentaje_lluvia = $request->procentaje_lluvia;
        $prons->save();
    }

    public function edit(Request $request)
    {
        $prons = Pronosticos::findOrFail($request->id);
        $prons->id_ciudad = $request->id_ciudad;
        $prons->id_climas = $request->id_climas;
        $prons->fecha_hora_inicial = $request->fecha_hora_inicial;
        $prons->fecha_hora_final = $request->fecha_hora_final;
        $prons->procentaje_lluvia = $request->procentaje_lluvia;
        $prons->save();
    }

    public function delete($id)
    {
        $prons = Pronosticos::findOrFail($id);

        $prons->delete();
    }

    public function ciudad($id)
    {
        $prons = Pronosticos::where('id_ciudad', $id)->with('ciudad', 'climas')->get();

        return response()->json($prons);
    }
}
