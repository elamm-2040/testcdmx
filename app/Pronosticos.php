<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pronosticos extends Model
{
    protected $table = 'pronosticos';

    protected $dateFormat = 'Ymd H:i:s';

    public function ciudad()
    {
        return $this->hasOne(Ciudad::class, 'id', 'id_ciudad');
    }

    public function climas()
    {
        return $this->hasOne(Climas::class, 'id', 'id_climas');
    }
}
