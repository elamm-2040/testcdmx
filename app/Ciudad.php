<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ciudad extends Model
{
    protected $table = 'ciudad';

    protected $fillable = [
        'nombre',
        'estado',
        'pais'
    ];

    public function pronosticos()
    {
        return $this->hasMany(Pronosticos::class, 'id_ciudad', 'id');
    }
}
